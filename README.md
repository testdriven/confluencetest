This repository has some tests written to test Atlassian Confluence page's restriction setting feature.

**Prerequisite:**

1. The environment running the tests should have java 1.8 or greater installed. 


**Running the test:**

Steps: 

* Checkout project

* Change the following public variables in `RestrictionTest` class:

     * `pageUnderTest`
     
     * `adminUsernameCredential`
     
     * `nonAdminUsernameCredential`

     ** Please note it is not a good idea to hard code test data, but I assumed those will be objects defined by other classes in the larger test suite.

* Run the tests:
```mvn test```

** Unexpected pop ups may interfere with the tests. Should be handled in the future improvements.

**Assumptions:**

I assumed this tests will be part of larger test suite, 
where there will be public methods available from other page objects for:

a. creating pages.

b. creating users

c. creating user groups

And that is why some of the test data needed for my tests were set as hardcoded variables.

I will move the test data such as pageUnderTest, adminUsernameCredential, nonAdminUsernameCredential to a `yaml file`.

Further improvements:
All the page object web elements can be defined in an yaml file, so that any change in future doesn't require any code change.

