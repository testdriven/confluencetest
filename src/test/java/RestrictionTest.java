import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Test;
import java.util.concurrent.TimeUnit;

/**This class will contain tests
 *to test restriction setting feature
 * of confluence pages
 */
public class RestrictionTest {
    public static final String pageUnderTest = "https://imranc.atlassian.net/wiki/spaces/AQ/pages/163842/Test+page";
    //set the adminUsernameCredential as username, email, password
    public static final String[] adminUsernameCredential={"test","nafisadipi@gmail.com","NoMistakes"};
    //set the nonAdminUsernameCredential as username, email, password
    public static final String[] nonAdminUsernameCredential={"imranc.libra","imranc.libra@gmail.com","nomistakes"};

    public static WebDriver loginAndOpenConfluencePage(String username, String password){
        System.setProperty("webdriver.chrome.driver","chromedriver");
        WebDriver driver = new ChromeDriver();
        driver.get(pageUnderTest);

        LoginPage login = new LoginPage(driver);
        login.loginAs(username,password);

        WebDriverWait wait2= new WebDriverWait(driver,20);
        wait2.until(ExpectedConditions.visibilityOfElementLocated(By.id("title-text")));
        wait2.until(ExpectedConditions.visibilityOfElementLocated(By.id("com-atlassian-confluence")));
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        return driver;
    }

    @Test
    public static void testAdminCanEditRestriction(){
        WebDriver driver = loginAndOpenConfluencePage(adminUsernameCredential[1],adminUsernameCredential[2]);
        RestrictionSettingPage rp = new RestrictionSettingPage(driver);
        rp.openRestrictionsDialog();
        rp.clickRestrictionsContainer();
        Assert.assertTrue(driver.findElement(rp.selectAnyoneCanViewEdit).isDisplayed(),"Restriction change made by admin was not successful.");
        driver.close();
    }

    @Test
    public static void testViewOnlySetFromEditingRestricted(){
        //This test will check when read only access is set
        // user cannot edit a page
    }

    @Test
    public static void editingRestrictedPermissionSetterCanEdit(){
        //When editing restricted is set, permission setter user
        //should be able to keep editing the page
    }

    @Test
    public static void testViewOnlySetFromVERestricted(){
        //This test will set view only permission for a user from "Viewing and editing restricted"
        // and check if user can edit
        WebDriver driver = loginAndOpenConfluencePage(adminUsernameCredential[1],adminUsernameCredential[2]);
        RestrictionSettingPage rp = new RestrictionSettingPage(driver);
        rp.setViewingAndEditingPermission(nonAdminUsernameCredential[0],Boolean.FALSE);

        driver.quit();

        WebDriver driver1 = loginAndOpenConfluencePage(nonAdminUsernameCredential[1],nonAdminUsernameCredential[2]);
        WikiPage wikiPage = new WikiPage();
        Assert.assertFalse(driver1.findElement(wikiPage.pageEditButton).isDisplayed(),"User with view permission only, can still edit.");
        driver1.quit();
    }
}
