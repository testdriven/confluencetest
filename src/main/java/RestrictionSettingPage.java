import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.concurrent.TimeUnit;

/**
 * This is a restriction setting page object
 * which contains necessary web elements and methods defined in it
 */

public class RestrictionSettingPage {
    WebDriver driver;

    public RestrictionSettingPage(WebDriver driver){
        this.driver=driver;
    }

    By restrictionsDialogButton = By.xpath("//button[@data-test-id='restrictions.dialog.button']");
    By restrictionDialogContent = By.xpath("//div[@data-test-id='restrictions-dialog.content-mode-select']");
    By selectAnyoneCanViewEdit = By.id("react-select-2-option-0");
    By selectEditingRestricted = By.id("react-select-2-option-1");
    By selectVERestricted = By.id("react-select-2-option-2");
    By applyChangeButton = By.xpath("//button//span[text()=\"Apply\"]");


    private void waitForElementToBeClickable(By id) {
        WebDriverWait wait = new WebDriverWait(driver, 100);
        wait.until(ExpectedConditions.elementToBeClickable(id));
    }

    public RestrictionSettingPage openRestrictionsDialog(){
        waitForElementToBeClickable(restrictionsDialogButton);
        driver.findElement(restrictionsDialogButton).click();
        //Not a good idea to inject implicit wait, will fix this later
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        return this;
    }

    public RestrictionSettingPage clickRestrictionsContainer(){
        waitForElementToBeClickable(restrictionDialogContent);
        driver.findElement(restrictionDialogContent).click();
        return this;
    }

    public void setUniversalAccess(){
        //This method sets "Anyone can view and edit" permission
    }

    public void applyPermissionChanges(){
        waitForElementToBeClickable(applyChangeButton);
        driver.findElement(applyChangeButton).click();
    }

    public void setEditingPermission(String usernameOrGroup){
        //This page object method sets view only for every user
        // and set edit permission for specific user name or group provided
    }

    public void removeEditingPermission(String usernameOrGroup){
        //This method removes edit permission set for user or group from "Editing restricted"
    }

    public void setViewingAndEditingPermission(String usernameOrGroup, Boolean canEdit){
        //This method will set "View only" or "View and Edit" permission for a specific user or group
        //By userAndGroupSearchDiv = By.xpath("//div[@data-test-id='restrictions-dialog.users-and-groups-search']");
        By userAndGroupSearchDivButton = By.xpath("//div[@data-test-id='restrictions-dialog.users-and-groups-search']//button");
        By userAndGroupSearch = By.xpath("//div[@data-test-id='user-and-group-search']");
        By userAndGroupSearchField = By.xpath("//div[@data-test-id='user-and-group-search']//input");
        By firstUserOrGroupInResult = By.xpath("//div[@id='react-select-3-option-0']/div[@data-test-id='user-group-search-label']");
        By dropDownCanViewAndEdit = By.xpath("//div[@data-test-id='restrictions-dialog.users-and-groups-search']//span[contains(text(),'Can view and edit')]");

        //Opens restrictionDialog and selects "Viewing and Editing restricted"
        openRestrictionsDialog();
        clickRestrictionsContainer();
        driver.findElement(selectVERestricted).click();

        //Types in a user or a group
        waitForElementToBeClickable(userAndGroupSearch);
        driver.findElement(userAndGroupSearch).click();
        driver.findElement(userAndGroupSearchField).sendKeys(usernameOrGroup);
        driver.findElement(firstUserOrGroupInResult).click();

        //Clicks on drop down to select can view or can view and edit.
        waitForElementToBeClickable(dropDownCanViewAndEdit);
        driver.findElement(dropDownCanViewAndEdit).click();

        //if canEdit is set to true, select can edit and view
        //if canEdit is set to false, select can view
        if (canEdit){
            driver.findElement(By.id("react-select-4-option-1")).click();
        }
        else if (!canEdit){
            driver.findElement(By.id("react-select-4-option-0")).click();
        }

        //Apply changes
        driver.findElement(userAndGroupSearchDivButton).click();
        applyPermissionChanges();
        //return this;
    }

    public void removeViewingAndEditingPermission(String usernameOrGroup){
        //This method removes permission set from "Viewing and Editing restricted"
    }
}
