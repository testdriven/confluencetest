import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * This is a login page object
 * which contains necessary web elements and methods defined in it
 */

public class LoginPage {
    WebDriver driver;
    By username = By.id("username");
    By password = By.id("password");
    By submit = By.id("login-submit");

    public LoginPage(WebDriver driver){
        this.driver=driver;
    }

    public void loginAs(String UserName, String Password){
        driver.findElement(username).sendKeys(UserName);
        driver.findElement(submit).click();
        //driver.findElement(password).wait();
        WebDriverWait wait2= new WebDriverWait(driver,10);
        wait2.until(ExpectedConditions.visibilityOfElementLocated(password));
        driver.findElement(password).sendKeys(Password);
        driver.findElement(submit).click();
    }
}
